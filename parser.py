import requests
from bs4 import BeautifulSoup
import csv

# 1. Выясняем кол-во страниц
# 2. Сформировать список url на страницы выдачи
# 3. Собрать данные

def get_html(url):
    r = requests.get(url)
    return r.text

def get_total_pages(html): #Функция определения количества страниц для парсинга по запросу
    soup = BeautifulSoup(html, 'lxml')
    pages = soup.find('div', class_='pagination-pages').find_all('a', class_ = 'pagination-page')[-1].get('href') #берем ссылку
    total_pages = pages.split('=')[1].split('&')[0]                  #последней страницы
    return int(total_pages)

def write_csv(data):    #функция записи в файл
    with open('avito.csv', 'a') as f:
        writer = csv.writer(f)

        writer.writerow( (data['title'],    #записываем словарь
                          data['price'],
                          data['metro'],
                          data['url']) )



def get_page_data(html): #Функция парсинга
    soup = BeautifulSoup(html, 'lxml')

    ads = soup.find('div', class_='catalog-list').find_all('div', class_='item_table')

    for ad in ads:  #цикл парсинга по все объявлениям на сттранице
        #title, price, metro, url

        name = ad.find('div', class_='description').find('h3').text.strip().lower()

        if 'iphone' in name: # Отбираем объявления содержащие iphone
            try:
                title = ad.find('div', class_='description').find('h3').text.strip()
            except:
                title = ''

            try:
                url = 'https://www.avito.ru' + ad.find('div', class_='description').find('h3').find('a').get('href')
            except:
                url = ''

            try:
                price = ad.find('div', class_='description').find('div', class_='about').text.strip()
            except:
                price = ''

            try:
                metro = ad.find('div', class_='description').find('div', class_='data').find_all('p')[-1].strip()
            except:
                metro = ''

            data = {'title': title,
                    'url': url,
                    'price': price,
                    'metro': metro}
            print(data)

            write_csv(data)
        else:
            continue


def main():
    url = 'https://www.avito.ru/moskva/telefony?p=2&q=Apple'
    base_url = 'https://www.avito.ru/moskva/telefony?' # Конструкор для ссылки - для пагинга
    page_part = 'p='                                   # Конструкор для ссылки - для пагинга
    query_part = '&q=Apple'                            # Конструкор для ссылки - для пагинга

    #total_pages = get_total_pages(get_html(url))

    #for i in range(1, total_pages + 1):
    for i in range(1, 2):
        url_gen = base_url + page_part + str(i) + query_part
        #print(url_gen)
        html = get_html(url_gen)
        get_page_data(html)

if __name__ == '__main__':
    main()
